<?php
/*
// mysql_connect("database-host", "username", "password")
$conn = mysql_connect("localhost","root","root") 
			or die("cannot connected");

// mysql_select_db("database-name", "connection-link-identifier")
@mysql_select_db("test",$conn);
*/

/**
 * mysql_connect is deprecated
 * using mysqli_connect instead
 */

$databaseHost = getenv('MYSQL_HOST')||'localhost';
$databaseName = getenv('MYSQL_DB')||'test';
$databaseUsername =getenv('MYSQL_USERNAME')||'root';
$databasePassword = getenv('MYSQL_PASSWORD')||'root';

$mysqli = mysqli_connect($databaseHost, $databaseUsername, $databasePassword, $databaseName); 
 
?>

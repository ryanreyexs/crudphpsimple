FROM debian
RUN apt update
RUN apt install php apache2 lib-apache2-mod-php php-mysql -y 
COPY web /var/www/html/
ENV MYSQL_HOST=172.17.0.2
ENV MYSQL_USERNAME=root
ENV MYSQL_PASSWORD=123
ENV MYSQL_DB=test
EXPOSE 80
CMD ["/bin/bash","-c","set -e;apachectl -D FOREGROUND \"$@\" "]
